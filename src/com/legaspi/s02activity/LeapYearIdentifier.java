package com.legaspi.s02activity;

import java.util.Scanner;

public class LeapYearIdentifier {
    public LeapYearIdentifier(){
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Check if a year if it's a leap year or not!");
        System.out.println("Enter year: \n");
        int year = input.nextInt();
        boolean isLeap = false;

        if(year % 4 == 0)
        {
            if( year % 100 == 0)
            {
                if ( year % 400 == 0)
                    isLeap = true;
                else
                    isLeap = false;
            }
            else
                isLeap = true;
        }
        else {
            isLeap = false;
        }

        if(isLeap==true)
            System.out.println(year + " is a leap year. Thanks for checking!");
        else
            System.out.println(year + " is not a leap year. Thanks for checking!");
    }
}
